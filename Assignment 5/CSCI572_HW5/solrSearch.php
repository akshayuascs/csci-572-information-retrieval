<script
  src="https://code.jquery.com/jquery-2.2.4.js"
  integrity="sha256-iT6Q9iMJYuQiMWNd9lDyBUStIq/8PuOW33aOqmvFpqI="
  crossorigin="anonymous"></script>
<script
  src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"
  integrity="sha256-T0Vest3yCU7pafRw9r+settMBX6JkKN06dqBnpQ8d30="
  crossorigin="anonymous"></script>
<style>
ul,#q.ui-autocomplete-input {
  list-style-type: none;
	background-color: aliceblue;
	width:15em;
}
.ui-helper-hidden-accessible {
	display:none;
}

</style>
<script>
$( function() {
	var URL = 'http://localhost:8983/solr/irhw4/suggest?q=';
	$('#myform').on('keyup', '#q', function(e){
			var lWord = $("#q").val().toLowerCase().split(" ").pop(-1);
                        var prevWord = "";
                        var multiWord = $("#q").val().split(" ");
		$( "#q" ).autocomplete({
			source: function( request, response ) {
				$.ajax( {
          				url: URL + lWord + "&wt=json",
					crossDomain: true,
		            		dataType : 'jsonp',
                	    		jsonp : 'json.wrf',
 					data: {
            					term: request.term
          				},
					success: function( data ) {
						response( data );
          					response($.map(data.suggest.suggest[lWord].suggestions, function(value, key) {
							if (multiWord.length > 1) {
                                    				var lIndex = $("#q").val().lastIndexOf(" ");
                                    				prevWord = $("#q").val().substring(0, lIndex + 1).toLowerCase();
                               				}
							return prevWord + value.term;                   
						})); 
					}
        			}); 
      			},
			minLength: 0,
    		});
  	});
});
</script>
<script>
var reClick = 0;
function reSearch(){
	reClick = 1;	
	document.getElementById("hid").value = 1;
	document.forms['myform'].submit();	
}
</script>
<?php 
ini_set('max_execution_time',600);
ini_set('memory_limit','3G');
include_once '/var/www/html/SpellCorrector.php';
include_once '/var/www/html/simple_html_dom.php'; 
?>
<?php
$flag = 0;
// make sure browsers see this page as utf-8 encoded HTML
header('Content-Type: text/html; charset=utf-8');
$limit = 10;
$query = isset($_REQUEST['q']) ? $_REQUEST['q'] : false;
$reqType = isset($_REQUEST['index']) ? $_REQUEST['index'] : lucene ;
$results = false;
if ($query)
{
	$query = strtolower($query);
	$queryForSolr = $query;
	ini_set('memory_limit', '-1');
	require_once('/var/www/html/Apache/Solr/Service.php');
	$solr = new Apache_Solr_Service('localhost', 8983, '/solr/irhw4/');
	if (get_magic_quotes_gpc() == 1)
	{
		$queryForSolr = stripslashes($queryForSolr);
	}
	$i = 0;
	$arrString = array();
     $arr =  explode(" ", $query);
     $queryAfterCorrection = "";
foreach($arr as $v){
    
    $queryAfterCorrection=$queryAfterCorrection.SpellCorrector::correct($v)." ";

}

	$queryBeforeCorrection = $queryForSolr;
	if($queryBeforeCorrection != $queryAfterCorrection){
		$queryForSolr = $queryAfterCorrection;
		$flag = 1;
		
	}
	if($reqType == pagerank){
		$additionalParameters = array(
		'sort' => 'pageRankFile desc'
		);
	}
	try{		
		?><script> var qBefore = '<?php echo $queryBeforeCorrection; ?>'; //alert(qBefore);</script><?php		
		$rc = $_REQUEST['hid'];
		if($rc){
			$queryForSolr = $queryBeforeCorrection;		
		}
		$results = $solr->search($queryForSolr, 0, $limit, $additionalParameters);
	}
	catch (Exception $e){
		die("<html><head><title>SEARCH EXCEPTION</title><body><pre>{$e->__toString()}</pre></body></html>");
	}
	
}

function getLinkfromMap($input) {
 $val = array_map('str_getcsv', file('/home/akshay/USAMap.csv'));
    foreach($val as $value) {
        if ($value[0] == $input) {
            return $value[1];
        }
    }
}
?>
<html>
	<head>
		<title>PHP Solr Client Example</title>
	</head>
	<body>
		<form id = "myform" accept-charset="utf-8" method="get">
		<div class="ui-widget">    
    
<center>
			<label for="q">Search:</label>
			<input id="q" name="q" type="text" value="<?php echo htmlspecialchars($query, ENT_QUOTES, 'utf-8'); ?>"/>
			<input type="radio" name = "index"  value="lucene"> Solr Lucene
			<input type="radio" name = "index" value="pagerank"> Pagerank
			<input id="hid" type="hidden" name="hid" value = "0">
			<input type="submit"/></center></div>
		</form>
<?php
if ($results)
{
if(trim($queryForSolr) != trim($queryBeforeCorrection)){
		echo '<span>Showing results for:</span>'; echo " "; echo "<span style='color:blue;'><b><i>$queryAfterCorrection</i></b></span>"."<br/>"; 
		echo '<span>Search instead for:</span>'; echo " "; ?><a href="#" onclick="reSearch(qBefore);" >
 		<?php  echo "<span style='color:blue;'><b><i>$queryBeforeCorrection</i></b></span>".'<br/><br/>' ?></a>  
	<?php ;}
	$total = (int) $results->response->numFound;
	$start = min(1, $total);
	$end = min($limit, $total);
?>
<div>Results <?php echo $start; ?> - <?php echo $end;?> of <?php echo $total; ?>:</div>
<ol>
<?php
foreach ($results->response->docs as $doc)
{
	$json = json_decode($doc, true);
	echo '<pre>';
	echo $json->id;

?>

<table style="border: 1px solid black; text-align: left"> 
<?php 
/*echo $queryBeforeCorrection;
echo $queryAfterCorrection;*/
foreach ($doc as $field => $value)
{?>
<?php { 
?>
<li>
	<tr> 
		<th>Title</th>
		<?php if($doc->title != ""){ ?>
		<td><a target = "_blank" href = "<?php echo htmlspecialchars($doc->og_url, ENT_NOQUOTES, 'utf-8');?>">
		<?php  echo $doc->title; ?></a></td>
		<?php }
		else {  ?>
		<td><?php echo "No Title"; }  ?></td>
	</tr>
	<tr> 
		<th>URL</th>
		<td><a target="_blank" href="<?php echo htmlspecialchars($doc->og_url, ENT_NOQUOTES, 'utf-8');?>">
		<?php $fileName = $doc->resourcename;
		
	        $MapFileIndex = substr($fileName, strrpos($fileName, '/') + 1); echo getLinkfromMap($MapFileIndex); /*echo "<br>"; echo $fileName;*/ ?></a></td>
	</tr>

	<tr> 
		<th>ID</th>
		<td><?php if($doc->id != "") {echo $doc->id;} else {echo "N/A";} ?></td></tr>
	<tr> 
		<th>Description</th><td><?php if($doc->description != "") {echo $doc->description;} else {echo "No Description";} ?></td></tr>
	<tr> <th>Snippet</th>
		<td>
		<?php 
		$fileContent = file_get_html($fileName)->plaintext;
         	$counter =0;
        	$sentenceArray = explode(".", $fileContent);
        	$flag = "false";
		$flagMulti = 1;
		$break_flag = 0;
		$break_flag1 = 0;
        	$length = count($sentenceArray);
        	for ($i = 0; $i < $length; $i++) {
        	     	$value = $sentenceArray[$i];
			if ($doc->title != ''){
		        	if (strpos(strtolower($value), strtolower($doc->title)) !== false) {
	                		continue;
        			}
    			}
			$queryToCheck = $queryAfterCorrection;
			$rc = $_REQUEST['hid'];
			if($rc){
				$queryToCheck = $queryBeforeCorrection;		
			}
        	     	if(stripos(strtolower($value),strtolower($queryToCheck))!== false) {
				$value1 = stripos($value, $queryAfterCorrection);
                    		echo substr($value,$value1,200);
		    		echo "...";			
                    		$flag ="true";
				$flagMulti = 0;
                    		break;
			}               
		}
		if($flagMulti == 1 && $flag == "false"){
			$query_list = explode(" ",$queryToCheck);
		        for ($i = 0; $i < $length; $i++) {
 				$value = $sentenceArray[$i];
	        		if ($doc->title != ''){ 
	        			if (strpos(strtolower($value), strtolower($doc->title)) !== false) { 
        	           			continue;
			  		} 
            			}
        			foreach($query_list as $q_val){
                	        //echo $q_val;
		    	            	$break_flag = 0;
            				if (strpos(strtolower($value), strtolower($q_val)) !== false) { 
			                $value1 = stripos($value, $q_val); 
                			$substring = substr($value, $value1,160); 
                			echo $substring; 
					$break_flag = 1;
                			echo "..."; 
 		
			                //echo $arr_html[$i]." "; 
                			$snippet_count += strlen($substring);
                			$flag = "true"; 

			                if($snippet_count > 160){
				                $break_flag = 1; 
				                break;
                			} 
 
            			}
				if($break_flag == 1){
			            break;
        			}
			}
			if($break_flag == 1){
            			break;

        		}
		}

		if($flag == "false"){
			$query_list = explode(" ",$queryToCheck);
		        for ($i = 0; $i < $length; $i++) {
				$value = $doc->description;
			        foreach($query_list as $q_val){
                        	//echo $q_val;
            				$break_flag = 0;
					if (strpos(strtolower($value), strtolower($q_val)) !== false) { 
			 	               $value1 = stripos($value, $q_val); 
                			       $substring = substr($value, $value1,160); 
				               echo $substring; 
                //echo $arr_html[$i]." "; 
				               echo "..."; 
 		
				               $snippet_count += strlen($substring);
				               $flag = "true"; 
						if($snippet_count > 160){
                $break_flag1 = 1; 
                break;
                } 
 

                //break; 
 
                }
if($break_flag1 == 1){
            break;

        }
else{
	echo " ";
	break;
}

}
 if($break_flag1 == 1){
            break;

        }
}

}


	}
        ?>  </td>
	</tr>
</li>
<?php }
break; } 
?>
</tbody>
</table>  
<?php
 }
?>
 </ol>
<?php
}
?>
</body>
</html>
