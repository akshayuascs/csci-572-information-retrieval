import java.util.*;
import org.apache.hadoop.mapreduce.lib.input.FileSplit;
import java.io.IOException;
import java.util.StringTokenizer;
import org.apache.hadoop.mapreduce.InputSplit;
import org.apache.hadoop.mapreduce.InputFormat;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.Mapper;
import org.apache.hadoop.mapreduce.Reducer;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;

public class InvertedIndexJob{

	public static class TokenizerMapper extends Mapper<LongWritable, Text, Text, Text>{
	private Text word = new Text();
	private String[] finalFName;
		public void map(LongWritable key, Text value, Context context) throws IOException,InterruptedException{
			String fileName = ((FileSplit)context.getInputSplit()).getPath().getName();
			finalFName = fileName.split("\\.");
			StringTokenizer itr = new StringTokenizer(value.toString());
			while (itr.hasMoreTokens()) {
				word.set(itr.nextToken());
				context.write(word, new Text(finalFName[0]));
				}
			}

		}	

	public static class Reduce extends Reducer<Text, Text, Text, Text> {
	public void reduce(Text key, Iterable<Text> values, Context context) throws IOException, InterruptedException { 
		HashMap newMap = new HashMap();
		int wordCount=0;
		for(Text t:values){
			String str=t.toString();
			if(newMap!=null &&newMap.get(str)!=null){
				wordCount=(int)newMap.get(str);
				newMap.put(str, ++wordCount);
			}else{
				newMap.put(str, 1);
			}
		}
	String new2 = newMap.toString();
	new2 = new2.replace('{', ' ');
	new2 = new2.replace('}', ' ');
	new2 = new2.replace('=', ':');
	new2 = new2.replace(',', ' ');
	context.write(key, new Text(new2));
	}
 
}

public static void main(String[] args) throws Exception {
    Configuration conf = new Configuration();
    Job job = Job.getInstance(conf, "word count");
    job.setJarByClass(InvertedIndexJob.class);
    job.setMapperClass(TokenizerMapper.class);
    job.setReducerClass(Reduce.class);
    job.setOutputKeyClass(Text.class);
    job.setOutputValueClass(Text.class);
    FileInputFormat.addInputPath(job, new Path(args[0]));
    FileOutputFormat.setOutputPath(job, new Path(args[1]));
    System.exit(job.waitForCompletion(true) ? 0 : 1);
  }


}
